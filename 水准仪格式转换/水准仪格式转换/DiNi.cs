﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 水准仪格式转换
{
    public class DiNi
    {
        private string _name;
        private string _view;
        private double _value;
        private double _length;
        private double _heigth;
        public string Name { get => _name; set => _name = value; }
        public string View { get => _view; set => _view = value; }
        public double Value { get => _value; set => _value = value; }
        public double Length { get => _length; set => _length = value; }
        public double Heigth { get => _heigth; set => _heigth = value; }

        public DiNi()
        { 
        }
        public DiNi(string name, string view, double value, double length,double heigth)
        {
            _name = name;
            _view = view;
            _value = value;
            _length = length;
            _heigth = heigth;
        }
    }
}
