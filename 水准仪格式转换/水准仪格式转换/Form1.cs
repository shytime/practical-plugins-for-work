﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace 水准仪格式转换
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //桌面位置
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            List<string> lineStr = new List<string>();
            List<string> lineStrNew = new List<string>();
            //打开文件对话框
            OpenFileDialog openFileDialog = new OpenFileDialog();
            try
            {
                openFileDialog.InitialDirectory = desktopPath;
                openFileDialog.Filter = "dat file(*.dat)|*.dat|txt file(*.txt)|*.txt|all file(*.*)|*.*";
                openFileDialog.Title = "数据文件打开对话框";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //首先按行提取txt文件中的数据
                    lineStr = File.ReadAllLines(openFileDialog.FileName).ToList();
                    //1、前2行去掉
                    lineStr.RemoveRange(0, 2);
                    foreach (var str in lineStr)
                    {
                        if (!str.Contains("#####") && !str.Contains("Measurement repeated") && !str.Contains("End-Line"))
                        {
                            lineStrNew.Add(str);
                        }
                    }
                }
                lineStrNew.RemoveRange(lineStrNew.Count - 3, 3);
            }
            catch (Exception)
            {
                throw;
            }
            //概略高程
            List<string> glgc = new List<string>();
            //点名
            List<string> dm = new List<string>();
            //临时数组，用于剔除概略高程
            List<string> tempRemovegc = new List<string>();
            //提取完概略高程之后将数据剔除
            foreach (var item in lineStrNew)
            {
                string[] temp = item.Split('|');
                if (temp[5].Contains("Z"))
                {
                    glgc.Add(temp[5].Trim().Replace("Z", ""));
                    dm.Add(ExtractStrings(temp[2])[1]);
                }
                if (!temp[5].Contains("Z"))
                {
                    tempRemovegc.Add(item);
                }
            }
            lineStrNew.Clear();
            lineStrNew.AddRange(tempRemovegc);

            List<DiNi> dini = new List<DiNi>();
            //提取每一行有用的信息
            foreach (var item in lineStrNew)
            {
                DiNi diNitemp = new DiNi();
                //用|将数据分割开
                string[] temp1 = item.Split('|');
                //点名
                string[] temp2 = ExtractStrings(temp1[2]);
                //照准方向 读数
                string[] temp3 = ExtractStrings(temp1[3]);
                //距离
                string[] temp4 = ExtractStrings(temp1[4]);
                diNitemp.Name = temp2[1];
                diNitemp.View = temp3[0];
                diNitemp.Value = Convert.ToDouble(temp3[1]);
                diNitemp.Length = Convert.ToDouble(temp4[1].Replace("m", ""));
                dini.Add(diNitemp);
            }
            //将DiNi格式中的概略高程提取出来
            for (int i = 0; i < dm.Count; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    if (dm[i] == dini[i * 4 + j].Name)
                    {
                        dini[i * 4 + j].Heigth = Convert.ToDouble(glgc[i].Replace("m", ""));
                    }
                }
            }
            //计算概略高程
            for (int i = 0; i < dini.Count() / 4; i++)
            {
                if (dini[4 * i].View == "Rb" && dini[4 * i].Heigth != 0)
                {
                    dini[4 * i + 1].Heigth = dini[4 * i].Value - dini[4 * i + 1].Value + dini[4 * i].Heigth;
                    dini[4 * i + 2].Heigth = dini[4 * i + 3].Value - dini[4 * i + 2].Value + dini[4 * i].Heigth;
                }
                else
                {
                    dini[4 * i].Heigth = dini[4 * i + 1].Value - dini[4 * i].Value + dini[4 * i + 1].Heigth;
                    dini[4 * i + 3].Heigth = dini[4 * i + 2].Value - dini[4 * i + 3].Value + dini[4 * i + 1].Heigth;
                }
            }
            //将文件格式修改为后前前后
            for (int i = 0; i < dini.Count/4; i++)
            {
                if (dini[4 * i].View == "Rf")
                {
                    DiNi temp1 = new DiNi();
                    DiNi temp2 = new DiNi();
                    temp1 = dini[4 * i];
                    dini[4 * i] = dini[4 * i + 1];
                    dini[4 * i + 1] = temp1;
                    temp2 = dini[4 * i + 2];
                    dini[4 * i + 2] = dini[4 * i + 3];
                    dini[4 * i + 3] = temp2;
                }
            }
            //输出文本
            for (int i = 0; i < dini.Count; i++)
            {
                string tempStr = i + 1 + "," + dini[i].Name + "0" + "," + "1" + "," + dini[i].View + "," + dini[i].Length + "," + dini[i].Value + "," + dini[i].Heigth + "\n";
                if (tempStr.Contains("Rb"))
                {
                    tempStr = tempStr.Replace("Rb", "1");
                }
                else
                {
                    tempStr = tempStr.Replace("Rf", "2");
                }
                FileAdd(openFileDialog.FileName.Replace(".", "索佳格式."), tempStr);
            }

        }
        /// <summary>
        /// 拆分一个或多个空格分割的字符串
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string[] ExtractStrings(string input)
        {
            string[] extractedStrings = Regex.Split(input, @"\s+");
            return extractedStrings;
        }
        public static void FileAdd(string path, string strings)
        {
            StreamWriter sw = File.AppendText(path);
            sw.Write(strings);
            sw.Flush();
            sw.Close();
            sw.Dispose();
        }

    }
}
